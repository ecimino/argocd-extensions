((window) => {
  const component = () => {
        return React.createElement(
          "div",
          { style: { padding: "10px" } },
          React.createElement("img", { src: "https://cataas.com/cat", alt: "Cat" })
      );
  };

  window.extensionsAPI.registerSystemLevelExtension(
      component,
      "Cat",
      "/cat",
      "fa-cat"
  );
})(window);
